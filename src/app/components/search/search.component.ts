import { Component, OnInit } from '@angular/core';
import {Heroe, HeroesService} from '../../services/heroes.service';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  heroes: Heroe[] = [];
  termino: string;

  constructor( private heroesService: HeroesService,
               private activatedRoute: ActivatedRoute,
               private router: Router ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe( params => {
      this.heroes = this.heroesService.buscarHeroes( params['id'] );
      this.termino = params['id'];
    });
    console.log(this.heroes);
  }

  verHeroe( idx: number ) {
    this.router.navigate( ['/heroe', idx] );
  }

}
