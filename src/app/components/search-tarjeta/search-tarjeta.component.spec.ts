import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchTarjetaComponent } from './search-tarjeta.component';

describe('SearchTarjetaComponent', () => {
  let component: SearchTarjetaComponent;
  let fixture: ComponentFixture<SearchTarjetaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchTarjetaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchTarjetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
