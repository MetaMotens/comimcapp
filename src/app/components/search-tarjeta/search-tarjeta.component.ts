import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'app-search-tarjeta',
  templateUrl: './search-tarjeta.component.html',
  styleUrls: ['./search-tarjeta.component.css']
})
export class SearchTarjetaComponent implements OnInit {

  @Input() index: number;
  @Input() heroe: any;

  constructor( private router: Router ) { }

  ngOnInit() {
  }

  verHeroe() {
    this.router.navigate(['/heroe', this.index]);
  }

}
